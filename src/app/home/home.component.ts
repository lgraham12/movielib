import { Media } from './../models/media';
import { MediaService } from './../media.service';
import { AuthService } from './../login/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  trendSub: Subscription = new Subscription;
  filterSub: Subscription = new Subscription;
  genresSub: Subscription = new Subscription;
  genreSelectSub: Subscription = new Subscription;

  genresList: {id: number, name: string}[] = [];
  selectedGenreId: number = 28;
  isLoading = false;
  p = 1;
  query: string = "";
  trendAll: any;
  mediaList: Media[] = [];

  constructor(private authService: AuthService, private mediaService: MediaService) {}

  ngOnInit(): void {
    this.authService.autoLogin();
    this.fetchAllPages();
    this.fetchAllGenres();
  }

  fetchAllGenres() {
    let i = 0;
    this.genresSub = this.mediaService.getAllGenres().subscribe(data => {
      while (i < data.genres.length){
        this.genresList.push(this.populateGenresList(data.genres[i]));
        i++;
      }
      console.log(this.genresList);
    });
  }

  populateGenresList(data : any) {
    let id: number = data.id;
    let name: string = data.name;
    return ({id, name});
  }

  filterByGenre() {
    this.isLoading = true;
    this.mediaList = [];
    this.genreSelectSub = this.mediaService.getMovieByGenre(this.selectedGenreId).subscribe( data  => {
      this.populateList(data);
      this.isLoading = false;
    })
  }

  filterByName(query: string) {
    this.isLoading = true;
    if (query.length >= 2) {
      this.mediaList = [];
      console.log(query);
      this.filterSub = this.mediaService.getSearchedMedia(query, 1).subscribe(responseList => {
          console.log(responseList);
          this.populateList(responseList);
      })
    }
    if (query.length < 2) {
      this.mediaList = [];
      this.fetchAllPages();
    }
    this.isLoading = false;

  }
  
  fetchAllPages() {
    let i = 0;
    this.isLoading = true;
    this.trendSub = this.mediaService.requestMultiplePages().subscribe(responseList => {
      while (i < responseList.length)
      {
        this.populateList(responseList[i]);
        i++;
      }
    this.isLoading = false;
    })
  }

  populateList(trendAll: any) {
    let i = 0;
    if (!trendAll || !trendAll.results || trendAll.results.length == 0)
    {
      // alert("An error occured");
      return ;
    }
    while (i < trendAll.results.length)
    {
      this.mediaList.push(this.populateItem(trendAll.results[i]));
      i++;
    }
  }

  populateItem(data: any) {
    let mediaItem: Media = {} as Media;
    mediaItem.title = data.name;
    mediaItem.overview = data.overview;
    mediaItem.releaseDate = data.first_air_date;
    mediaItem.imagePath = this.mediaService.getPosterUrl(data.poster_path);
    mediaItem.popularity = data.popularity;
    return mediaItem;
  }

  ngOnDestroy() {
    this.trendSub.unsubscribe();
    this.filterSub.unsubscribe();
    this.genresSub.unsubscribe();
    this.genreSelectSub.unsubscribe();
  }
}