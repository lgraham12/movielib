import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { environment } from 'src/environments/environment';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { DualRingSpinnerComponent } from './Shared/load-spinners/dual-ring-spinner/dual-ring-spinner.component';
import { ItemListComponent } from './item-list/item-list.component';
import { FavoriteListComponent } from './favorite-list/favorite-list.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AuthGuard } from './login/auth.guard';
import { AuthService } from './login/auth.service';
import { ItemCardComponent } from './item-card/item-card.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    HomeComponent,
    DualRingSpinnerComponent,
    ItemListComponent,
    FavoriteListComponent,
    LandingPageComponent,
    ItemCardComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgSelectModule
  ],
  providers: [
    AuthService,
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
