export interface Media {
    title: string;
    overview: string;
    releaseDate: string;
    imagePath: string;
    popularity: number;
    isFavored?: boolean;
}