import { LandingPageComponent } from './landing-page/landing-page.component';
import { AuthGuard } from './login/auth.guard';
import { HomeComponent } from './home/home.component';
// import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { ItemListComponent } from './item-list/item-list.component';
import { FavoriteListComponent } from './favorite-list/favorite-list.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LandingPageComponent },
  { path: 'signup', component: LandingPageComponent},
  { path: 'movies', component: ItemListComponent },
  { path: 'series', component: ItemListComponent },
  { path: 'favorites', component: FavoriteListComponent },
  { path: '**', component: LandingPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
