import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  constructor(private http: HttpClient) { }


  //hardcoded 10pages of results, gotta set this dynamically.
  requestMultiplePages(): Observable<any[]> {
    let page1 = this.getAllMedia(1);
    let page2 = this.getAllMedia(2);
    let page3 = this.getAllMedia(3);
    let page4 = this.getAllMedia(4);
    let page5 = this.getAllMedia(5);
    let page6 = this.getAllMedia(6);
    let page7 = this.getAllMedia(7);
    let page8 = this.getAllMedia(8);
    let page9 = this.getAllMedia(9);
    let page10 = this.getAllMedia(10);

    return forkJoin([page1, page2, page3, page4, page5, page6, page7, page8, page9, page10]);
  }

  getAllMedia(pageNumber: number) {
    return this.http.get('https://api.themoviedb.org/3/trending/tv/day?api_key=0f60ad592a39d4b497a0d8889bba1be2&page=' + pageNumber);
  }

  getPosterUrl(path: string) {
    if (path)
      return ('https://image.tmdb.org/t/p/w342/' + path);
    return ('https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg');
  }

  getSearchedMedia(query: string, pageNumber: number) {
    return this.http.get('https://api.themoviedb.org/3/search/movie?query=' + query + '&api_key=0f60ad592a39d4b497a0d8889bba1be2&page=' + pageNumber);
  }

  getAllGenres(): Observable<any> {
    return this.http.get('https://api.themoviedb.org/3/genre/movie/list?api_key=0f60ad592a39d4b497a0d8889bba1be2&language=en-US');
  }

  getMovieByGenre(id: number) {
    return this.http.get('https://api.themoviedb.org/3/discover/movie?api_key=0f60ad592a39d4b497a0d8889bba1be2&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=' + id);
  }
}
